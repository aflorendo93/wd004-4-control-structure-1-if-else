//This function converts a number into words
// @param {number} number // The number we want to convert
//@return {string}	converted number to words

// 1. Create an array of words where we will get the converted value
//Goal 1: Convert 0-9
const oneDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"];

//Goal 2: Convert 10-90 (Multiple of 10)
// 10, 20, 30, 40, 50, 60, 70, 80, 90
// Divide the data by 10, and you'll get the index - 1
const tens = ["", "Ten", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];

//Goal 3: We have to make sure that Goal#2 will only check numebers less than 100

//Goal 4: 11-19

//Goal 5: All other numbers less than 100;
const teens = ["", "Eleven","Twelve","Thirteen","Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"];


function converter(number) {
	//Goal 1
	if(number <= 9){
		return oneDigit[number];
	}

	//Goal 2
	//to check if a number is a multiple of ten, the remainder when the numer is divided by 10 should be zero;
	//We added another condition to make sure that the number wll be fivisible by 10 and less than 100
	if(number%10 === 0 && number < 100){
		const tensIndex = number/10;
		return tens[tensIndex];
	}

	if(number > 10 && number < 20){
		//We can get the "index" by subtracting 10 from the number
		const teensIndex = number - 10;
		return teens[teensIndex];
	}

	//Goal 5, -3/4 - thirty four
	//The goal of this function is to separate the first (tensDigit) digit and the last digit (onesIndex). Use them as an index to get the value from the previously created arrays.
	if (number < 100){
		//to get the last digit, we will get the remainder of the number when divided by 10 (%10)
		const onesIndex = number % 10;
		//to get the first digit, we need to divide the number by 10. But make sure fist that ther will be no remainders
		const numberWithoutRemainder = number - onesIndex;
		const tensIndex = numberWithoutRemainder / 10;
		return tens[tensIndex] + " " + oneDigit[onesIndex];
	}

	// the keyword return will terminate the function so therefore the tasks below the return statement will not be executed

//For 100, 200, 300
	if(number%100 === 0 && number < 1000){
		 const hundredsNum = number/100;
		 return oneDigit[hundredsNum] + " Hundred";
	}

	
//For Hundred Elevens to Nineteens
	if (number < 1000) {
	if(number%100 >=11 && number%100 <= 19){
		const teenHunNum = number%100;
		const teenHunNumDig = teenHunNum - 10;
		const teenHunNumHun = number - teenHunNum;
		const teenHunNumHunDig = teenHunNumHun/100;
		return oneDigit[teenHunNumHunDig] +" Hundred "+ teens[teenHunNumDig];
	}
}


// for 121, 221.....
	if((((number%100)/10)%1) !== 0 && number < 1000){
		const hunTensNumInit = number%100;
		const hunOnesDig = hunTensNumInit%10;
		const hunTensNumInitTo = hunTensNumInit - hunOnesDig;
		const hunTensNum = hunTensNumInitTo/10;
		const hunsNumInit = number - hunTensNumInit;
		const hunsNum = hunsNumInit/100;
		return oneDigit[hunsNum] +" Hundred "+ tens[hunTensNum] + " " + oneDigit[hunOnesDig]; 
}


// For 110, 120...
	if((((number%100)/10)%1) === 0 && number < 1000){
		const hunTensNumInit = number%100;
		const hunTensNum = hunTensNumInit/10;
		const hunsNumInit = number - hunTensNumInit;
		const hunsNum = hunsNumInit/100;
		return oneDigit[hunsNum] +" Hundred "+ tens[hunTensNum];
 }

// For 1000, 2000...
		if(number%1000 === 0 && number < 10000){
		 const thouNum = number/1000;
		 return oneDigit[thouNum] + " Thousand";
	}

const remainderThou = number % 1000;
const remainderHun = remainderThou % 100;
//Thousand one to Thousand ninety-nine
	if (number >= 1000 && remainderThou < 100 && number < 10000) {
		if (remainderThou < 10) {
			const ones = number % 1000;
			const thouInit = number - ones;
			const thou = thouInit / 1000;
			return oneDigit[thou] + " Thousand " + oneDigit[ones];
		} else if (remainderThou%10 === 0 && remainderThou < 99) {
			const sampInit = number % 1000;
			const samp = sampInit/10;
			const thouInit = number - sampInit;
			const thou = thouInit / 1000;
			return oneDigit[thou] + " Thousand " + tens[samp];
		} 	else if (remainderThou >= 11 && remainderThou <=19) {
			const teenageInit = number % 1000;
			const teenage = teenageInit - 10;
			const thouInit = number - teenageInit;
			const thou = thouInit / 1000;
			return oneDigit[thou] + " Thousand " + teens[teenage];

		} else if (remainderThou%10 !== 0 && remainderThou < 99) {
				const digitInit = number % 1000;
				const digit = digitInit % 10;
				const sampInit = digitInit - digit;
				const samp = sampInit / 10;
				const thouInit = number - digitInit;
				const thou = thouInit / 1000;
				return oneDigit[thou] + " Thousand " + tens[samp] + " " + oneDigit[digit];
}
}


//Thousand Hundred tens digit
		if(number >= 1000 && remainderThou >= 100 && number < 10000){
		if(remainderHun >= 11 && remainderHun <= 19) {
		const thouHunNums = number % 1000;
		const thoutenNumIn = thouHunNums % 100;
		const thoutenNum = thoutenNumIn - 10;
		const hundredsIn = thouHunNums - thoutenNumIn;
		const hundreds = hundredsIn / 100;
		const thousandsIn = number - thouHunNums;
		const thousands = thousandsIn/1000;
		return oneDigit[thousands] + " Thousand " + oneDigit[hundreds] + " Hundred " + teens[thoutenNum];

	} else if ((((number%1000)%100)%10) === 0){
		 const thouHunNums = number % 1000;
		 const thoutenNum = thouHunNums % 100;
		 const thoutenNumFin = thoutenNum / 10;
		 const thouHunNumR = thouHunNums - thoutenNum;
		 const thouHunNumFinFin = thouHunNumR/100;
		 const thouNumm = number - thouHunNums;
		 const thouNumFF = thouNumm / 1000;
		 return oneDigit[thouNumFF] + " Thousand " + oneDigit[thouHunNumFinFin] + " Hundred " + tens[thoutenNumFin];
	} else {
		 const thouHunNums = number % 1000;
		 const thoutenNum = thouHunNums % 100;
		 const thoudigNum = thoutenNum % 10;
		 const thoutenNumR = thoutenNum - thoudigNum;
		 const thoutenNumFin = thoutenNumR / 10;
		 const thouHunNumR = thouHunNums - thoutenNum;
		 const thouHunNumFinFin = thouHunNumR/100;
		 const thouNumm = number - thouHunNums;
		 const thouNumFF = thouNumm / 1000;
		 return oneDigit[thouNumFF] + " Thousand " + oneDigit[thouHunNumFinFin] + " Hundred " + tens[thoutenNumFin] + " " +oneDigit[thoudigNum];
	
}
}
}











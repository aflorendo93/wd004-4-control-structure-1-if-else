// Conditions -  these are statements that will either result to true or false;
//
//if
//if(condition){
	//task you want to execute
//}
//
// It will only execute the task if the condition is true



//if-else
// if(condition){
	//task you want to execute if the condition is true
//} else {
	//task you want to execute if the condition = false;
//}

function checkAge(age){
	if(age>18){
		return "Legal ka na.";
	} else {
		return "Minor kapa";
	}
}

// This function checks the age and returns a string
//@param (number) age User-input age

function whereYouShouldbe(age) {
	//0-5 with Mommy/Daddy
	//6-12 Elementary School
	//13-18 High School
	//19-22 College
	//22-23 Soul Searching
	//24-50 Work
	//51-Enjoying life
	if(age <= 5) {
		return "With Mommy/Daddy";
	} else if(age <=12){
		return "Elementary School";
	} else if(age <=18){
		return "High School";
	} else if(age <=22){
		return "College";
	} else if(age <=23){
		return "Soul Searching";
	} else if(age <=50){
		return "Work";
	} else (age >=51)
		return "Enjoying Life";
}

// if-else if- .... else;

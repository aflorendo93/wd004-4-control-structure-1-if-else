// Arrays - A collection of related data. [];
//
//const fruits = ["Apple", "Banana", "Kiwi"].
//index is the position of the data relative to the next data, therefore it starts counting from zero.
//Array manipulation methods;
//.push(newData) - add/end
//.unshift(newdata) - add/starts

//.pop() - delete / end
//.shift() - delete / start
//
//.splice(startingIndex, deleteCount, optional---- data we want to insert)

// == -> equality operator/compares regardless of the data
// ==== -> Strict equality operator / compares the values and the data types.
//
// != -> inequality operator/compares values regardless of data types
// !== -> strict inequality operator/compare the values and the data types
//
// <,>, <=, >=
